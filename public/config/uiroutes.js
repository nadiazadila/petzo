angular.module('angularApp')
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider.state({
            name: 'login',
            url: '/login',
            controller: 'loginCtrl',
            templateUrl: 'views/login.html'
        })
        .state({
            name: 'dashboard',
            url: '/dashboard',
            template: '<h3>this is dashboard~</h3>',
            // controller: 'loginCtrl',
            // templateUrl: 'views/login.html'
        })
        .state({
            name: 'register',
            url: '/register',
            // controller: 'loginCtrl',
            templateUrl: 'views/registerUser.html'
        });
        $urlRouterProvider.otherwise('/login');
    });