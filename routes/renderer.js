var express = require('express');
var router = express.Router();

// router.get('/', function (req, res) {
// 	return res.send("Hello opeNode!");
// });

router.get('/api', (request, response) => {
    response.status(200).send({message: 'Hello World!'})
});

module.exports = router;
